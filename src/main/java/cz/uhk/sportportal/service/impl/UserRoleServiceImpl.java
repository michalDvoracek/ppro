package cz.uhk.sportportal.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.uhk.sportportal.dao.UserRoleDao;
import cz.uhk.sportportal.model.UserRole;
import cz.uhk.sportportal.service.UserRoleService;

@Service
@Transactional
public class UserRoleServiceImpl implements UserRoleService {
	
	@Autowired
    private UserRoleDao userRoleDao;

	@Override
	public List<UserRole> findAll() {
		return userRoleDao.findAll();
	}


	@Override
	public void save(UserRole role) {
		userRoleDao.save(role);
	}

	@Override
	public UserRole update(UserRole userRole) {
		return userRoleDao.update(userRole);
	}

	@Override
	public UserRole remove(UserRole userRole) {
		return userRoleDao.remove(userRole);
	}

	@Override
	public UserRole findById(Integer id) {
		return userRoleDao.findById(id);
	}

	@Override
	public int getCount() {
		return userRoleDao.getCount();
	}

	@Override
	public UserRole findUserRoleByName(String name) {return userRoleDao.findUserRoleByName(name);}
}
