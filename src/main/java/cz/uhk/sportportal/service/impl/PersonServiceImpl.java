package cz.uhk.sportportal.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.uhk.sportportal.dao.PersonDao;
import cz.uhk.sportportal.model.Person;
import cz.uhk.sportportal.model.Team;
import cz.uhk.sportportal.service.PersonService;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonDao personDao;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public List<Person> findAll() {
        return personDao.findAll();
    }
    
    


    @Override
    public void save(Person person) {
        person.setPassword(passwordEncoder.encode(person.getPassword()));
        personDao.save(person);
    }

    @Override
    public Person update(Person person) {
        return personDao.update(person);
    }

    @Override
    public Person remove(Person person) {
        return personDao.remove(person);
    }

    @Override
    public Person findById(Integer p) {
        return personDao.findById(p);
    }

    @Override
    public int getCount() {
        return personDao.getCount();
    }

    @Override
    public List<Person> findByTeam(Integer id) {
        return personDao.findByTeam(id);
    }

    @Override
    public void setNoTeam(Person person) {
        personDao.setNoTeam(person);
    }

    @Override
    public Person findByUsername(String username) {
        return personDao.findByUsername(username);
    }

    @Override
    public void updatePersons(List<Person> personList) {
        personDao.updatePersons(personList);
    }

	@Override
	public List<Person> findPersonWithPoints() {
		return personDao.findPersonWithPoints();
	}


}
