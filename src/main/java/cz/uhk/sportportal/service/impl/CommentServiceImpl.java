package cz.uhk.sportportal.service.impl;

import cz.uhk.sportportal.dao.CommentDao;
import cz.uhk.sportportal.model.Comment;
import cz.uhk.sportportal.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CommentServiceImpl implements CommentService{

    @Autowired
    private CommentDao commentDao;

    @Override
    public List<Comment> findAll() {
        return commentDao.findAll();
    }

    @Override
    public void save(Comment comment) {
    	commentDao.save(comment);
    }

    @Override
    public Comment update(Comment comment) {
        return commentDao.update(comment);
    }

    @Override
    public Comment findById(Integer p) {
        return commentDao.findById(p);
    }

    @Override
    public int getCount() {
        return commentDao.getCount();
    }

	@Override
	public Comment remove(Comment t) {
		return commentDao.remove(t);
	}

	@Override
	public List<Comment> findByMatch(Integer id) {
		return commentDao.findByMatch(id);
	}

	@Override
	public List<Comment> findByPerson(Integer id) {
		return commentDao.findByPerson(id);
	}
}
