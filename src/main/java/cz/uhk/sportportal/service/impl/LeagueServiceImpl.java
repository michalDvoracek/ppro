package cz.uhk.sportportal.service.impl;

import cz.uhk.sportportal.dao.LeagueDao;
import cz.uhk.sportportal.model.League;
import cz.uhk.sportportal.service.LeagueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class LeagueServiceImpl implements LeagueService {

    @Autowired
    private LeagueDao leagueDao;

    @Override
    public List<League> findAll() {
        return leagueDao.findAll();
    }

    @Override
    public void save(League league) {
        leagueDao.save(league);
    }

    @Override
    public League update(League league) {
        return leagueDao.update(league);
    }

    @Override
    public League findById(Integer id) {
        return leagueDao.findById(id);
    }

    @Override
    public int getCount() {
        return leagueDao.getCount();
    }

	@Override
	public League remove(League league) {
		return leagueDao.remove(league);
	}

	@Override
	public List<Integer> findAllId() {
		return leagueDao.findAllId();
	}

	@Override
	public List<String> findAllNames() {
		return leagueDao.findAllNames();
	}


}
