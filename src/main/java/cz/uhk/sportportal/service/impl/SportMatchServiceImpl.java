package cz.uhk.sportportal.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.uhk.sportportal.dao.LeagueDao;
import cz.uhk.sportportal.dao.SportMatchDao;
import cz.uhk.sportportal.model.SportMatch;
import cz.uhk.sportportal.service.SportMatchService;

@Service
@Transactional
public class SportMatchServiceImpl implements SportMatchService {
	
	 @Autowired
	    private SportMatchDao sportMatchDao;

	@Override
	public List<SportMatch> findAll() {
		return sportMatchDao.findAll();
	}

	@Override
	public void save(SportMatch match) {
		sportMatchDao.save(match);
	}

	@Override
	public SportMatch update(SportMatch t) {
		return sportMatchDao.update(t);
	}

	@Override
	public SportMatch remove(SportMatch t) {
		return sportMatchDao.remove(t);
	}

	@Override
	public SportMatch findById(Integer p) {
		return sportMatchDao.findById(p);
	}

	@Override
	public int getCount() {
		return sportMatchDao.getCount();
	}

	@Override
	public List<SportMatch> findByTeam(Integer id) {
		return sportMatchDao.findByTeam(id);
	}

	@Override
	public List<SportMatch> findLastFiveMatch() {
		return sportMatchDao.findLastFiveMatch();
	}

	@Override
	public List<SportMatch> findNextFiveMatch() {
		return sportMatchDao.findNextFiveMatch();
	}

}
