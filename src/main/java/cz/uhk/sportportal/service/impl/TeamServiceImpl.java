package cz.uhk.sportportal.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.uhk.sportportal.dao.TeamDao;
import cz.uhk.sportportal.model.League;
import cz.uhk.sportportal.model.Team;
import cz.uhk.sportportal.service.TeamService;

@Service
@Transactional
public class TeamServiceImpl  implements TeamService {
	

    @Autowired
    private TeamDao teamDao;

	@Override
	public List<Team> findAll() {
		return teamDao.findAll();
	}

	@Override
	public void save(Team team) {
		teamDao.save(team);
	}

	@Override
	public Team update(Team t) {
		return teamDao.update(t);
	}

	@Override
	public Team remove(Team t) {
		return teamDao.remove(t);
	}

	@Override
	public Team findById(Integer p) {
		return teamDao.findById(p);
	}

	@Override
	public int getCount() {
		return teamDao.getCount();
	}

	@Override
	public List<Team> findByLeague(Integer id) {
		return teamDao.findByLeague(id);
	}

	@Override
	public List<Team> findFiveBest() {
		return teamDao.findFiveBest();
	}
}
