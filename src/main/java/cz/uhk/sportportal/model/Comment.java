package cz.uhk.sportportal.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.Size;

@Entity
public class Comment {

    @Id
    @GeneratedValue
    private int id;

    private Date created;

    @Size(min = 3)
    private String text;
    
	@OneToOne
    private Person person;

    @OneToOne
    private SportMatch sportmatch;

    @OneToOne
    private Team team;

	@Override
	public String toString() {
		return "Comment{" +
				"id=" + id +
				", created=" + created +
				", text='" + text + '\'' +
				", person=" + person +
				", sportmatch=" + sportmatch +
				", team=" + team +
				'}';
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public SportMatch getSportmatch() {
		return sportmatch;
	}

	public void setSportmatch(SportMatch sportmatch) {
		this.sportmatch = sportmatch;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}


}
