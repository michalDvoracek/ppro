package cz.uhk.sportportal.controller;

import cz.uhk.sportportal.model.League;
import cz.uhk.sportportal.model.Team;
import cz.uhk.sportportal.service.LeagueService;
import cz.uhk.sportportal.service.TeamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;


@Controller
@Configuration
@ComponentScan("cz.uhk.sportportal.service")
public class LeagueController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    LeagueService leagueService;

    @Autowired
    TeamService teamService;

    @ModelAttribute("league")
    public League construct() {
        return new League();
    }

    /**
     * Simply selects the home view to render by returning its name.
     */

    @RequestMapping(value = "/leagues")
    public String leagues(Model model) {

        model.addAttribute("leagues", leagueService.findAll());
        model.addAttribute("leguesize", leagueService.getCount());

        return "leagues";
    }

    @RequestMapping(value = "/league-detail")
    public String leagueDetail(Model model, @RequestParam("id") int id) {
        model.addAttribute("league", leagueService.findById(id));
        model.addAttribute("teams", teamService.findByLeague(id));
        return "league-detail";
    }

    @RequestMapping(value = "/new-league", method = RequestMethod.GET)
    public String newLeague(Model model) {
        League league = new League();
        model.addAttribute("league", league);
        return "new-league";
    }

    @RequestMapping(value = "/new-league", method = RequestMethod.POST)
    public String saveNewLeague(Model model, @Valid @ModelAttribute("league") League league, BindingResult result) {
        if (result.hasErrors()) {
            logger.error("Binding error: " + result);
            return newLeague(model);
        }

        try {
            leagueService.save(league);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/leagues";
    }

    @RequestMapping(value = "/league-remove")
    public String removeLeague(Model model, @RequestParam("id") int id) {
        League league = leagueService.findById(id);

        List<Team> teamsForLeague = teamService.findByLeague(id);

        if (teamsForLeague.size() > 0)
        {
            //TODO: ERROR cannot delete league with team in it
        } else {
            leagueService.remove(league);

        }

        return "redirect:/leagues";
    }

    @RequestMapping(value = "/league-edit", method = RequestMethod.GET)
    public String editLeague(Model model, @RequestParam("id") int id) {
        League league = leagueService.findById(id);
        model.addAttribute("league", league);
        return "league-edit";
    }

    @RequestMapping(value = "/league-edit", method = RequestMethod.POST)
    public String editLeague(Model model, @Valid @ModelAttribute("league") League league, BindingResult result) {
        if (result.hasErrors()) {
            logger.error("Binding error: " + result);
            return leagueDetail(model, league.getId());
        }
        try {
            leagueService.update(league);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/league-detail?id="+league.getId();
    }
}