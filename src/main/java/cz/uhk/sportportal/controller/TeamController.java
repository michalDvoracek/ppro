package cz.uhk.sportportal.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cz.uhk.sportportal.model.League;
import cz.uhk.sportportal.model.Person;
import cz.uhk.sportportal.model.SportMatch;
import cz.uhk.sportportal.model.Team;
import cz.uhk.sportportal.service.LeagueService;
import cz.uhk.sportportal.service.PersonService;
import cz.uhk.sportportal.service.SportMatchService;
import cz.uhk.sportportal.service.TeamService;

@Controller
@Configuration
@ComponentScan("cz.uhk.sportportal.service")
public class TeamController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	TeamService teamService;
	@Autowired
	LeagueService leagueService;
	@Autowired
	PersonService personService;
	@Autowired
	SportMatchService matchService;
	
	@RequestMapping(value = "/teams")
    public String teams(Model model) {
        model.addAttribute("teams", teamService.findAll());
        return "teams";
    }
	
	@RequestMapping(value = "/team-detail")
    public String teamDetail(Model model, @RequestParam("id") int id) {
        model.addAttribute("team", teamService.findById(id));
        model.addAttribute("persons", personService.findByTeam(id));
        model.addAttribute("matches", matchService.findByTeam(id));
        return "team-detail";
    }
	
    @RequestMapping(value = "/new-team", method = RequestMethod.GET)
    public String newTeam(Model model){
    	Team team = new Team();
        model.addAttribute("leagues", leagueService.findAll());   
        model.addAttribute("team", team);
    	return "new-team";
    }
   
    @RequestMapping(value = "/new-team", method = RequestMethod.POST)
    public String saveNewTeam(Model model, @Valid @ModelAttribute("team") Team team, BindingResult result){
        if (result.hasErrors()) {
            logger.error("Binding error: " + result);
            return newTeam(model);
        }
        try {       	 
           teamService.save(team);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/teams";
    }
    
    @RequestMapping(value = "/team-remove")
    public String removeTeam(Model model, @RequestParam("id") int id) {
    	
        Team team = teamService.findById(id);
        List<Person> personsInTeam = personService.findByTeam(id);
        List<SportMatch> teamMatch = matchService.findByTeam(id);
        
        if (personsInTeam.size() > 0 || teamMatch.size() > 0){
        	for (Person person : personsInTeam){
        		personService.setNoTeam(person);
        	}
        	for (SportMatch match : teamMatch){
        		matchService.remove(match);
        	}
        	
        } else {
        	teamService.remove(team);
        }
        return "redirect:/teams";
    }

    @RequestMapping(value = "/team-edit", method = RequestMethod.GET)
    public String editTeam(Model model, @RequestParam("id") int id) {
        Team team = teamService.findById(id);
        model.addAttribute("team", team);
        model.addAttribute("leagues", leagueService.findAll());
        return "team-edit";
    }

    @RequestMapping(value = "/team-edit", method = RequestMethod.POST)
    public String editTeam(Model model, @Valid @ModelAttribute("team") Team team, BindingResult result) {
        if (result.hasErrors()) {
            logger.error("Binding error: " + result);
            return teamDetail(model, team.getId());
        }
        try {
            teamService.update(team);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/team-detail?id="+team.getId();
    }
    
    
    
	
		
}
