package cz.uhk.sportportal.controller;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import cz.uhk.sportportal.model.Person;
import cz.uhk.sportportal.model.Team;
import cz.uhk.sportportal.service.LeagueService;
import cz.uhk.sportportal.service.PersonService;
import cz.uhk.sportportal.service.SportMatchService;
import cz.uhk.sportportal.service.TeamService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@Configuration
@ComponentScan("cz.uhk.sportportal.service")
public class HomeController {

	@Autowired
	LeagueService leagueService;
	@Autowired
	PersonService personService;
	@Autowired
	SportMatchService matchService;
	@Autowired
	TeamService teamService;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Locale locale, Model model) {

		List<Person> persons = personService.findPersonWithPoints();
		model.addAttribute("persons", persons);
		model.addAttribute("matchesPast", matchService.findLastFiveMatch());
		model.addAttribute("teams", teamService.findFiveBest() );
		model.addAttribute("matchesFuture", matchService.findNextFiveMatch());
		
		return "home";
	}
}