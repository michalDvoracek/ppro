package cz.uhk.sportportal.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import cz.uhk.sportportal.model.*;
import cz.uhk.sportportal.service.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Configuration
@ComponentScan("cz.uhk.sportportal.service")
public class PersonController {
    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    PersonService personService;
    @Autowired
    TeamService teamService;
    @Autowired
    CommentService commentService;
    @Autowired
    UserRoleService userRoleService;
    @Autowired
    SportMatchService matchService;

    @RequestMapping(value = "/persons")
    public String persons(Model model) {
        model.addAttribute("persons", personService.findAll());
        return "persons";
    }

    @RequestMapping(value = "/person-detail")
    public String personDetail(Model model, @RequestParam("id") int id) {
        model.addAttribute("person", personService.findById(id));
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        boolean showMoreDetail = false;
        if (username != "anonymousUser") {
            Person loggedUser = personService.findByUsername(username);
            Person displayedPerson = personService.findById(id);
            if (displayedPerson.getTeam() != null && loggedUser.getTeam() != null) {
                if ((loggedUser.getUserRole().getId() == 1)) {
                    showMoreDetail = true;
                }
                if (loggedUser.getTeam().getId() == displayedPerson.getTeam().getId()){
                    showMoreDetail = true;
                }
                model.addAttribute("showMoreDetail", showMoreDetail);
            }
        }
        return "person-detail";
    }

    @RequestMapping(value = "/new-person", method = RequestMethod.GET)
    public String newPerson(Model model) {
        Person person = new Person();
        model.addAttribute("teams", teamService.findAll());
        model.addAttribute("person", person);
        return "new-person";
    }

    @RequestMapping(value = "/new-person", method = RequestMethod.POST)
    public String saveNewPerson(Model model, @Valid @ModelAttribute("person") Person person, BindingResult result) {
        if (result.hasErrors()) {
            logger.error("Binding error: " + result);
            return newPerson(model);
        }
        try {
            personService.save(person);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/persons";
    }

    @RequestMapping(value = "/person-remove")
    public String removePerson(Model model, @RequestParam("id") int id) {
        Person person = personService.findById(id);
        List<Comment> personComments = commentService.findByPerson(id);
        if (personComments.size() > 0) {
            for (Comment comment : personComments) {
                commentService.remove(comment);
            }
            personService.remove(person);
        } else {
            personService.remove(person);
        }
        return "redirect:/persons";
    }

    @RequestMapping(value = "/person-edit", method = RequestMethod.GET)
    public String editPerson(Model model, @RequestParam(value = "id", required = false) Integer id) {
        Person person;
        if (id == null) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            person = personService.findByUsername(username);
        } else {
            person = personService.findById(id);
        }

        model.addAttribute("person", person);
        model.addAttribute("teams", teamService.findAll());
        model.addAttribute("userRoles", userRoleService.findAll());
        return "person-edit";
    }

    @RequestMapping(value = "/person-edit", method = RequestMethod.POST)
    public String editPersonPost(Model model,
                                 @ModelAttribute("person") Person person,
                                 @ModelAttribute("userRole.id") Integer userRoleId) {
        person.setUserRole(userRoleService.findById(userRoleId));
        personService.update(person);
        return "redirect:/person-detail?id=" + person.getId();
    }


    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String showRegistrationPage(Model model) {
        Person person = new Person();
        model.addAttribute("person", person);
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String saveRegisteredPerson(@Valid @ModelAttribute("person") Person person, Model model) {

        person.setUserRole(userRoleService.findUserRoleByName("ROLE_USER"));
        person.setPoints(0);
        personService.save(person);

        return "redirect:/registration-success";
    }

    @RequestMapping(value = "/registration-success", method = RequestMethod.GET)
    public String showRegistrationSucces() {
        return "registration-success";
    }

    @RequestMapping(value = "/my-team")
    public String showMyTeamPage(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();

        Person person = personService.findByUsername(username);
        if (person.getTeam() != null) {
            Team team = teamService.findById(person.getTeam().getId());

            model.addAttribute("team", teamService.findById(team.getId()));
            model.addAttribute("persons", personService.findByTeam(team.getId()));
            model.addAttribute("matches", matchService.findByTeam(team.getId()));
        } else {
            model.addAttribute("team", null);
            model.addAttribute("persons", new ArrayList<Person>());
            model.addAttribute("matches", new ArrayList<SportMatch>());
        }

        return "my-team";
    }

}