package cz.uhk.sportportal.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import cz.uhk.sportportal.model.*;
import cz.uhk.sportportal.service.*;
import org.omg.CORBA.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Scope("session")
@Controller
@Configuration
@ComponentScan("cz.uhk.sportportal.service")
public class SportMatchController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    SportMatchService sportMatchService;
    @Autowired
    TeamService teamService;
    @Autowired
    CommentService commentService;
    @Autowired
    PersonService personService;
    @Autowired
    LeagueService leagueService;

    @RequestMapping(value = "/matches")
    public String matches(Model model) {
        model.addAttribute("matches", sportMatchService.findAll());
        model.addAttribute("today", new Date());
        return "matches";
    }

    @RequestMapping(value = "/match-detail", method = RequestMethod.GET)
    public String matchDetail(Model model, @RequestParam("id") int id) {
        Comment comment = new Comment();
        model.addAttribute("comm", comment);
        model.addAttribute("match", sportMatchService.findById(id));
        model.addAttribute("comments", commentService.findByMatch(id));
        model.addAttribute("today", new Date());
        return "match-detail";
    }

    @RequestMapping(value = "/match-detail", method = RequestMethod.POST)
    public String matchDetailAddComment(@RequestParam("id") int matchId, @Valid @ModelAttribute("comm") Comment comment) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();

        if (username != "anonymousUser") {
            Person loggedUser = personService.findByUsername(username);
            comment.setPerson(loggedUser);

            comment.setSportmatch(sportMatchService.findById(matchId));
            comment.setCreated(new Date());

            commentService.save(comment);
        }
        return "redirect:/match-detail?id=" + matchId;
    }

    @RequestMapping(value = "/new-match", method = RequestMethod.GET)
    public String newMatch(Model model) {
        SportMatch match = new SportMatch();
        model.addAttribute("leagues", leagueService.findAll());
        model.addAttribute("teams", teamService.findAll());
        model.addAttribute("match", match);
        return "new-match";
    }

    @RequestMapping(value = "/new-match", method = RequestMethod.POST)
    public String saveNewMatch(Model model, @Valid @ModelAttribute("match") SportMatch match, BindingResult result) {
        match.setAwayGoals(0);
        match.setAwayGoals(0);

        sportMatchService.save(match);
        return "redirect:/matches";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/setPerson-points", params = {"homeid", "awayid"}, method = RequestMethod.GET)
    public String setPointsForMatch(Model model,
                                    @ModelAttribute("homeid") int homeid,
                                    @ModelAttribute("awayid") int awayid,
                                    @ModelAttribute("matchid") int matchid) {
        List<Person> homePlayers;
        homePlayers = personService.findByTeam(homeid);
        Team homeTeam = teamService.findById(homeid);
        model.addAttribute("homeTeam", homeTeam);
        model.addAttribute("homePlayers", homePlayers);
        List<Person> awayPlayers;
        awayPlayers = personService.findByTeam(awayid);
        Team awayTeam = teamService.findById(awayid);
        model.addAttribute("awayTeam", awayTeam);
        model.addAttribute("awayPlayers", awayPlayers);
        model.addAttribute("sportMatch", new SportMatch());

        return "setPerson-points";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/setPerson-points", method = RequestMethod.POST)
    public String savePointsForMatch(Model model, HttpServletRequest request,
                                     @ModelAttribute("homeid") int homeid,
                                     @ModelAttribute("awayid") int awayid,
                                     @ModelAttribute("matchid") int matchid) {
        String[] pointsAway = request.getParameterValues("pointsAway");
        String[] pointsHome = request.getParameterValues("pointsHome");
        String[] playerAwayIds = request.getParameterValues("playerAwayId");
        String[] playerHomeIds = request.getParameterValues("playerHomeId");

        int goalsAway = 0;
        int goalsHome = 0;

        List<Person> homePlayers = personService.findByTeam(homeid);
        List<Person> awayPlayers = personService.findByTeam(awayid);

        List<Person> personsToUpdate = new ArrayList<Person>();

        for (int i = 0; i < pointsAway.length; i++) {
            if (pointsAway[i] != "") {
                for (Person person : awayPlayers) {
                    if (person.getId() == Integer.parseInt(playerAwayIds[i])) {
                        person.setPoints(person.getPoints() + Integer.parseInt(pointsAway[i]));
                        personsToUpdate.add(person);
                        goalsAway += Integer.parseInt(pointsAway[i]);
                    }
                }
            }
        }
        for (int i = 0; i < pointsHome.length; i++) {
            if (pointsHome[i] != "") {
                for (Person person : homePlayers) {
                    if (person.getId() == Integer.parseInt(playerHomeIds[i])) {
                        person.setPoints(person.getPoints() + Integer.parseInt(pointsHome[i]));
                        personsToUpdate.add(person);
                        goalsHome = goalsHome + Integer.parseInt(pointsHome[i]);
                    }
                }
            }
        }

        SportMatch match = sportMatchService.findById(matchid);
        match.setHomeGoals(goalsHome);
        match.setAwayGoals(goalsAway);

        personService.updatePersons(personsToUpdate);

        return "redirect:/matches";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/match-remove")
    public String removeMatch(Model model, @RequestParam("id") int id) {
        SportMatch match = sportMatchService.findById(id);
        List<Comment> matchComments = commentService.findByMatch(id);
        if (matchComments.size() <= 0) {
            sportMatchService.remove(match);
        } else {
            for (Comment comment : matchComments) {
                commentService.remove(comment);
            }
            sportMatchService.remove(match);
        }
        return "redirect:/matches";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/match-edit", method = RequestMethod.GET)
    public String editMatch(Model model, @RequestParam("id") int id) {
        SportMatch match = sportMatchService.findById(id);
        model.addAttribute("match", match);
        return "match-edit";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/match-edit", method = RequestMethod.POST)
    public String editMatch(Model model, @Valid @ModelAttribute("match") SportMatch match, BindingResult result) {
        if (result.hasErrors()) {
            logger.error("Binding error: " + result);
            return matchDetail(model, match.getId());
        }
        try {
            sportMatchService.update(match);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/match-detail?id=" + match.getId();
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/comment-remove")
    public String deleteComment(@ModelAttribute("id") int id,
                                @ModelAttribute("matchid") int matchid){
        Comment comment = commentService.findById(id);
        commentService.remove(comment);
        return "redirect:/match-detail?id=" + matchid;
    }
}
