package cz.uhk.sportportal.dao;

import java.util.List;

import cz.uhk.sportportal.model.Comment;

public interface CommentDao extends GenericDao<Comment, Integer> {
	
	public List<Comment> findByMatch (Integer id);
	public List<Comment> findByPerson (Integer id);

}