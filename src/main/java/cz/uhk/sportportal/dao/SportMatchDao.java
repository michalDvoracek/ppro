package cz.uhk.sportportal.dao;

import java.util.List;

import cz.uhk.sportportal.model.SportMatch;

public interface SportMatchDao extends GenericDao<SportMatch, Integer> {
	public List<SportMatch> findByTeam (Integer id);
	public List<SportMatch> findLastFiveMatch();
	public List<SportMatch> findNextFiveMatch();
	
}
