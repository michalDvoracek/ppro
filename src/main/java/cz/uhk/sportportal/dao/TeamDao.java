package cz.uhk.sportportal.dao;


import java.util.List;

import cz.uhk.sportportal.model.League;
import cz.uhk.sportportal.model.Team;

public interface TeamDao extends GenericDao<Team, Integer> {
	public List<Team> findByLeague(Integer id);
	public List<Team> findFiveBest();
}
