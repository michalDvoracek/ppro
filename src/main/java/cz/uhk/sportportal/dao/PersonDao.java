package cz.uhk.sportportal.dao;

import java.util.List;

import cz.uhk.sportportal.model.Person;
import cz.uhk.sportportal.model.Team;

public interface PersonDao extends GenericDao<Person, Integer> {
	
	public List<Person> findByTeam (Integer id);
	public void setNoTeam(Person person);
	public Person findByUsername(String username);
	public void updatePersons(List<Person> personList);
	public List<Person> findPersonWithPoints();

}
