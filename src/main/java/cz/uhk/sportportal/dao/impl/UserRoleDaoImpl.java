package cz.uhk.sportportal.dao.impl;

import cz.uhk.sportportal.dao.UserRoleDao;
import cz.uhk.sportportal.model.UserRole;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRoleDaoImpl implements UserRoleDao{
	
	private static final Logger logger = LoggerFactory.getLogger(LeagueDaoImpl.class);
	private SessionFactory sessionFactory;
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
    	this.sessionFactory = sessionFactory;
	}
	
    @Override
    public List<UserRole> findAll() {
        List<UserRole> userRoles = new ArrayList<UserRole>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        userRoles = session.createQuery("from UserRole").list();
        session.beginTransaction().commit();
        return userRoles;
    }

    @Override
    public void save(UserRole userRole) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(userRole);
        session.beginTransaction().commit();
    }

    @Override
    public UserRole update(UserRole userRole) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.update(userRole);
        session.beginTransaction().commit();
        return userRole;
    }

    @Override
    public UserRole findById(Integer p) {
    	UserRole userRole;
    	List<UserRole> userRoles = new ArrayList<UserRole>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        userRoles = session.createQuery("from UserRole u where u.id = " + p).list();
        if (!userRoles.isEmpty()){
        	userRole = userRoles.get(0);
        } else {
        	userRole = null;
        }
        session.beginTransaction().commit();
        return userRole;
    }

    @Override
    public int getCount() {
    	 List<UserRole> userRoles = new ArrayList<UserRole>();
         Session session = this.sessionFactory.getCurrentSession();
         session.beginTransaction();
         userRoles = session.createQuery("from UserRole").list();
        session.beginTransaction().commit();
         return userRoles.size();
    }

	@Override
	public UserRole remove(UserRole userRole) {
		Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.delete(userRole);
        session.beginTransaction().commit();
        return userRole;
	}

    @Override
    public UserRole findUserRoleByName(String name) {
        UserRole userRole;
        List<UserRole> userRoles;
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        userRoles = session.createQuery("from UserRole u where u.name = '" + name + "'").list();
        if (!userRoles.isEmpty()){
            userRole = userRoles.get(0);
        } else {
            userRole = null;
        }
        session.beginTransaction().commit();
        return userRole;
    }
}
