package cz.uhk.sportportal.dao.impl;

import cz.uhk.sportportal.dao.SportMatchDao;
import cz.uhk.sportportal.model.League;
import cz.uhk.sportportal.model.SportMatch;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SportMatchDaoImpl implements SportMatchDao{
	
	 private static final Logger logger = LoggerFactory.getLogger(LeagueDaoImpl.class);

	    private SessionFactory sessionFactory;

	    @Autowired
	    public void setSessionFactory(SessionFactory sessionFactory) {
	        this.sessionFactory = sessionFactory;
	    }
	
    @Override
    public List<SportMatch> findAll() {
    	List<SportMatch> matches = new ArrayList<SportMatch>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        matches = session.createQuery("from SportMatch").list();
        session.beginTransaction().commit();
        return matches;
    }

    @Override
    public void save(SportMatch sportMatch) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(sportMatch);
        session.beginTransaction().commit();
    }

    @Override
    public SportMatch update(SportMatch sportMatch) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(sportMatch);
        session.beginTransaction().commit();
        return sportMatch;
    }

    @Override
    public SportMatch findById(Integer p) {
    	SportMatch sportMatch;
    	List<SportMatch> matches = new ArrayList<SportMatch>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        matches = session.createQuery("from SportMatch s where s.id = " + p).list();
        if (!matches.isEmpty()){
        	sportMatch = matches.get(0);
        } else {
        	sportMatch = null;
        }
        session.beginTransaction().commit();
        return sportMatch;
    }

    @Override
    public int getCount() {
    	List<SportMatch> matches = new ArrayList<SportMatch>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        matches = session.createQuery("from SportMatch").list();
        session.beginTransaction().commit();
        return matches.size();
    }

	@Override
	public SportMatch remove(SportMatch sportMatch) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.delete(sportMatch);
        session.beginTransaction().commit();
        return sportMatch;
	}

	@Override
	public List<SportMatch> findByTeam(Integer id) {
    	List<SportMatch> matches = new ArrayList<SportMatch>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        matches = session.createQuery("from SportMatch s where s.homeTeam.id =" + id + "or s.awayTeam.id =" + id).list();
        session.beginTransaction().commit();
        return matches;
	}

	@Override
	public List<SportMatch> findLastFiveMatch() {
		List<SportMatch> matches = new ArrayList<SportMatch>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        Criteria cr = session.createCriteria(SportMatch.class);
        Calendar c = Calendar.getInstance();
        cr.add(Restrictions.lt("dateOfMatch", c.getTime()));
        cr.addOrder(Order.desc("dateOfMatch"));
        cr.setMaxResults(5);
        matches = cr.list();
        return matches;
	}

	@Override
	public List<SportMatch> findNextFiveMatch() {
		List<SportMatch> matches = new ArrayList<SportMatch>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        Criteria cr = session.createCriteria(SportMatch.class);
        Calendar c = Calendar.getInstance();
        cr.add(Restrictions.gt("dateOfMatch", c.getTime()));
        cr.addOrder(Order.desc("dateOfMatch"));
        cr.setMaxResults(5);
        matches = cr.list();
        return matches;
	}
}
