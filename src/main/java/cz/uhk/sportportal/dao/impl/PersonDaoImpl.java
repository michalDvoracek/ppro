package cz.uhk.sportportal.dao.impl;

import cz.uhk.sportportal.dao.PersonDao;
import cz.uhk.sportportal.model.League;
import cz.uhk.sportportal.model.Person;
import cz.uhk.sportportal.model.Team;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PersonDaoImpl implements PersonDao {
	
	
    private static final Logger logger = LoggerFactory.getLogger(LeagueDaoImpl.class);

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public List<Person> findByTeam (Integer id){
    	List<Person> persons = new ArrayList<Person>();
    	Session session = this.sessionFactory.getCurrentSession();
    	session.beginTransaction();
    	persons = session.createQuery("from Person p where p.team.id =" + id).list();
        session.beginTransaction().commit();
    	return persons;
    }

    
	
    @Override
    public List<Person> findAll() {
    	List<Person> persons = new ArrayList<Person>();
    	Session session = this.sessionFactory.getCurrentSession();
    	session.beginTransaction();
    	persons = session.createQuery("from Person p where p.firstname <> null or p.lastname <> null").list();
        session.beginTransaction().commit();
    	return persons;
    }

    @Override
    public void save(Person person) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(person);
        session.beginTransaction().commit();
    }

    @Override
    public Person update(Person person) {
    	Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.update(person);
        session.beginTransaction().commit();
        return person;
    }

    @Override
    public Person findById(Integer id) {
    	Person person;
    	List<Person> persons = new ArrayList<Person>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        persons = session.createQuery("from Person p where p.id = " + id).list();
        if (!persons.isEmpty()){
        	person = persons.get(0);
        } else {
        	person = null;
        }
        session.beginTransaction().commit();
        return person;
    }

    @Override
    public int getCount() {
        List<League> persons = new ArrayList<League>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        persons = session.createQuery("from Person").list();
        session.beginTransaction().commit();
        return persons.size();
    }

	@Override
	public Person remove(Person person) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.delete(person);
        session.beginTransaction().commit();
		return person;
	}
	
	@Override
	public void setNoTeam(Person person){
		Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.createQuery("update Person p set team = NULL where p.id = " + person.getId());
        session.beginTransaction().commit();
	}

    @Override
    public Person findByUsername(String loggedUser) {
        Person person;
        List<Person> persons;
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        persons = session.createQuery("from Person p where p.username = '" + loggedUser + "' " ).list();
        if (!persons.isEmpty()){
            person = persons.get(0);
        } else {
            person = null;
        }
        session.beginTransaction().commit();
        return person;
    }

    @Override
    public void updatePersons(List<Person> personList) {
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        for(Person person : personList){
            session.update(person);
        }
        session.beginTransaction().commit();
    }

	@Override
	public List<Person> findPersonWithPoints() {
    	List<Person> persons = new ArrayList<Person>();
    	Session session = this.sessionFactory.getCurrentSession();
    	session.beginTransaction();
    	Criteria cr = session.createCriteria(Person.class);
    	cr.add(Restrictions.isNotNull("points"));
    	cr.addOrder(Order.desc("points"));
    	cr.setMaxResults(5);
    	persons = cr.list();
    	return persons;
	}
}
