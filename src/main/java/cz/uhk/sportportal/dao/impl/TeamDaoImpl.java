package cz.uhk.sportportal.dao.impl;

import cz.uhk.sportportal.dao.TeamDao;
import cz.uhk.sportportal.model.League;
import cz.uhk.sportportal.model.Team;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TeamDaoImpl implements TeamDao{
	
	   private static final Logger logger = LoggerFactory.getLogger(LeagueDaoImpl.class);

	    private SessionFactory sessionFactory;

	    @Autowired
	    public void setSessionFactory(SessionFactory sessionFactory) {
	        this.sessionFactory = sessionFactory;
	    }
	
    @Override
    public List<Team> findAll() {
    	List<Team> teams = new ArrayList<Team>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        teams = session.createQuery("from Team").list();
        session.beginTransaction().commit();
        return teams;
    }

    @Override
    public void save(Team team) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(team);
        session.beginTransaction().commit();
    }

    @Override
    public Team update(Team team) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.update(team);
        session.beginTransaction().commit();
        return team;
    }

    @Override
    public Team findById(Integer p) {
    	Team team;
    	List<Team> teams = new ArrayList<Team>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        teams = session.createQuery("from Team t where t.id = " + p).list();
        if (!teams.isEmpty()){
        	team = teams.get(0);
        } else {
        	team = null;
        }
        session.beginTransaction().commit();
        return team;
    }

    @Override
    public int getCount() {
    	List<Team> teams = new ArrayList<Team>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        teams = session.createQuery("from Team").list();
        session.beginTransaction().commit();
        return teams.size();
    }

	@Override
	public Team remove(Team team) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.delete(team);
        session.beginTransaction().commit();
        return team;
	}

	@Override
	public List<Team> findByLeague(Integer id) {
		List<Team> teams = new ArrayList<Team>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        teams = session.createQuery("from Team t where t.league.id = " + id).list();
        session.beginTransaction().commit();
		return teams;
	}

	@Override
	public List<Team> findFiveBest() {
		List<Team> teams = new ArrayList<Team>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        teams = session.createQuery("from Team t order by t.points desc").setMaxResults(5).list();
        session.beginTransaction().commit();
        return teams;
	}

}
