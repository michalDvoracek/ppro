package cz.uhk.sportportal.dao.impl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cz.uhk.sportportal.dao.CommentDao;
import cz.uhk.sportportal.model.Comment;
import org.springframework.stereotype.Repository;

@Repository
public class CommentDaoImpl implements CommentDao {
	
	private static final Logger logger = LoggerFactory.getLogger(LeagueDaoImpl.class);
	
    private SessionFactory sessionFactory;
    
    @Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
    	this.sessionFactory = sessionFactory;
	}

	@Override
	public List<Comment> findAll() {
        List<Comment> comments = new ArrayList<Comment>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        comments = session.createQuery("from Comment").list();
		session.beginTransaction().commit();
        return comments;
	}

	@Override
	public void save(Comment comment) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(comment);
        session.beginTransaction().commit();
	}

	@Override
	public Comment update(Comment comment) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.update(comment);
        session.beginTransaction().commit();
        return comment;
	}

	@Override
	public Comment remove(Comment comment) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.delete(comment);
		session.beginTransaction().commit();
        return comment;
	}

	@Override
	public Comment findById(Integer id) {
    	Comment comment;
    	List<Comment> comments = new ArrayList<Comment>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        comments = session.createQuery("from Comment c where c.id = " + id).list();
        if (!comments.isEmpty()){
        	comment = comments.get(0);
        } else {
        	comment = null;
        }
		session.beginTransaction().commit();
        return comment;
	}

	@Override
	public int getCount() {
        List<Comment> comments = new ArrayList<Comment>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        comments = session.createQuery("from Comment").list();
		session.beginTransaction().commit();
        return comments.size();
	}

	@Override
	public List<Comment> findByMatch(Integer id) {
		List<Comment> comments = new ArrayList<Comment>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        comments = session.createQuery("from Comment c where c.sportmatch.id = " + id).list();
		session.beginTransaction().commit();
		return comments;
	}

	@Override
	public List<Comment> findByPerson(Integer id) {
		List<Comment> comments = new ArrayList<Comment>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        comments = session.createQuery("from Comment c where c.person.id = " + id).list();
		session.beginTransaction().commit();
		return comments;
	}

}
