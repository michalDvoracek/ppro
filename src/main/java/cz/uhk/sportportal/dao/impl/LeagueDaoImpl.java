package cz.uhk.sportportal.dao.impl;

import cz.uhk.sportportal.dao.LeagueDao;
import cz.uhk.sportportal.model.League;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class LeagueDaoImpl implements LeagueDao{

    private static final Logger logger = LoggerFactory.getLogger(LeagueDaoImpl.class);

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<League> findAll() {
        List<League> leagues = new ArrayList<League>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        leagues = session.createQuery("from League").list();
        session.beginTransaction().commit();
        return leagues;
    }

    @Override
    public void save(League league) {
        logger.info("League saved successfully: " + league.getName() + league.getId());
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(league);
        session.beginTransaction().commit();
    }

    @Override
    public League update(League league) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.update(league);
        session.beginTransaction().commit();
        return league;
    }

    @Override
    public League findById(Integer p) {
    	League league;
    	List<League> leagues = new ArrayList<League>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        leagues = session.createQuery("from League l where l.id = " + p).list();
        if (!leagues.isEmpty()){
        	league = leagues.get(0);
        } else {
        	league = null;
        }
        session.beginTransaction().commit();
        return league;
    }

    @Override
    public int getCount() {
        List<League> leagues = new ArrayList<League>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        leagues = session.createQuery("from League").list();
        session.beginTransaction().commit();
        return leagues.size();
    }

	@Override
	public League remove(League league) {
    	Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.delete(league);
        session.beginTransaction().commit();
		return league;
	}

	@Override
	public List<Integer> findAllId() {
        List<Integer> leagues = new ArrayList<Integer>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        leagues = session.createQuery("select id from League").list();
        session.beginTransaction().commit();
        return leagues;
	}

	@Override
	public List<String> findAllNames() {
        List<String> leagues = new ArrayList<String>();
        Session session = this.sessionFactory.getCurrentSession();
        session.beginTransaction();
        leagues = session.createQuery("select name from League").list();
        session.beginTransaction().commit();
        return leagues;
	}
}
