package cz.uhk.sportportal.dao;

import java.util.List;

import cz.uhk.sportportal.model.League;
import cz.uhk.sportportal.model.Person;

public interface LeagueDao extends GenericDao<League, Integer> {
	public List<Integer> findAllId ();
	public List<String> findAllNames ();
}
