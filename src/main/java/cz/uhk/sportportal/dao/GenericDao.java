package cz.uhk.sportportal.dao;

import java.util.List;

public interface GenericDao<T, PK> {
	public List<T> findAll();
	public void save(T t);
	public T update(T t);
	public T remove(T t);
	public T findById(PK p);
	public int getCount();

}
