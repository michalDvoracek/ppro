<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<form:form class="form-horizontal" modelAttribute="sportMatch" method="post">
    <div id="page-wrapper">
        <div class="row">
            <h3>Domácí: ${homeTeam.name} ${homeTeam.city}</h3>
        </div>
        <div class="row">
            <h4> ${homeTeam.league.name}</h4>
            <h5> Body: ${homeTeam.points}</h5>
        </div>
        <div class="row">
            <h3>Hráči</h3>
            <table width="100%" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>Jméno</th>
                    <th>Přijmení</th>
                    <th>Góly za zápas</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${homePlayers}" var="homePlayer">
                    <tr class="odd gradeX">
                        <td> ${homePlayer.firstname} </td>
                        <td> ${homePlayer.lastname} </td>
                        <td>
                            <input name="pointsHome" placeholder="0" class="form-control input-md" type="number">
                            <input name="playerHomeId" type="hidden" value="${homePlayer.id}">
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="row">
            <h3>Hosté: ${awayTeam.name} ${awayTeam.city}</h3>
        </div>
        <div class="row">
            <h4> ${awayTeam.league.name}</h4>
            <h5> Body: ${awayTeam.points}</h5>
        </div>
        <div class="row">
            <h3>Hráči</h3>
            <table width="100%" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>Jméno</th>
                    <th>Přijmení</th>
                    <th>Pozice</th>
                    <th>Góly za zápas</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${awayPlayers}" var="awayPlayer">
                    <tr class="odd gradeX">
                        <td> ${awayPlayer.firstname} </td>
                        <td> ${awayPlayer.lastname} </td>
                        <td>
                            <input name="pointsAway" placeholder="0" class="form-control input-md" type="number">
                            <input name="playerAwayId" type="hidden" value="${awayPlayer.id}">
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <input class="btn btn-primary submit-button" type="submit" value="Zadat výsledek"/>
            </div>
        </div>
    </div>
</form:form>


