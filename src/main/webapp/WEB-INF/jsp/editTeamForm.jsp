<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<form:form cssClass="form-horizontal" commandName="team" method="post">
<fieldset>

<!-- Form Name -->
<legend>Úprava týmu</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="name">Název Týmu</label>  
  <div class="col-md-4">
  	<form:input path="name" id="name" name="name" placeholder="název týmu" class="form-control input-md" required="" type="text" />
  	<span class="help-block">Zadejte název týmu</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="city">Město</label>  
	<div class="col-md-4">
  		<form:input id="city" path="city" name="city" placeholder="Město" class="form-control input-md" required="" type="text"/>
  		<span class="help-block">Zadejte město týmu</span>  
	</div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email</label>  
  <div class="col-md-4">
  <form:input id="email" path="email" placeholder="team@email.com" cssClass="form-control input-md" required="" typ="text"/>
  <span class="help-block">Zadejte mailovou adresu</span>  
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
	<label class="col-md-4 control-label" for="league" >Liga</label>
	<div class="col-md-4">
		<form:select id="league" name="league" path="league.id"  cssClass="form-control">
			<form:options id="league" name="league" items="${leagues}" itemValue="id" itemLabel="name"></form:options>
		</form:select>
	</div>
</div>
<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="create"></label>
  <div class="col-md-4">
    <input id="create" class="btn btn-primary" type="submit" value="Upravit"/>
  </div>
</div>
</fieldset>
</form:form>

