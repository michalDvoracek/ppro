<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div id="page-wrapper">
	<div class ="row">
		<div class="col-md-11">
			<h3>${league.name}</h3>	
		</div>
		<div class="col-md-1">
		    <a class="fa fa-pencil fa-fw" href="<spring:url value="/league-edit?id=${league.id}" />" ></a>
    		<a class="fa fa-trash fa-fw" href="<spring:url value="/league-remove?id=${league.id}" />" ></a>
		</div>

	</div>
	<div>
		<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
			<thead>
        		<tr>
            		<th>Tým</th>
                	<th>Město</th>
                	<th>Body</th>
           		</tr>
			</thead>
        	<tbody>
				<c:forEach items="${teams}" var="team">
					<tr class="odd gradeX">
                        <td><a href="<spring:url value="/team-detail?id=${team.id}" />">${team.name}</a></td>
                        <td><a href="<spring:url value="/team-detail?id=${team.id}" />">${team.city}</a></td>
                        <td><a href="<spring:url value="/team-detail?id=${team.id}" />">${team.points}</a></td>	
				</c:forEach>
			</tbody>
    	</table>
	</div>
</div>
