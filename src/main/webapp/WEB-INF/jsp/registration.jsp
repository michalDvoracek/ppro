<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<form:form cssClass="form-horizontal" modelAttribute="person" method="post">
    <div class="login-form">
        <div class="form-signin">
            <div class="col-md-12">
                <h2 class="form-signin-heading">Registrace do systému</h2>
                <form:input id="username" path="username" placeholder="Přihlašovací jméno"
                            cssClass="form-control input-md" required="" type="text"/>
                <span class="help-block">Zadejte uživatelské jméno</span>
                <form:input id="password" path="password" placeholder="heslo" cssClass="form-control input-md"
                            required="" type="password"/>
                <span class="help-block">Zadejte heslo</span>
                <form:input id="email" path="email" placeholder="osoba@email.com" cssClass="form-control input-md"
                            required="" typ="text"/>
                <span class="help-block">Zadejte mailovou adresu</span>
                <button class="btn btn-lg btn-primary btn-block col-md-12" type="submit">
                    Registrovat se
                </button>
            </div>
        </div>
    </div>


</form:form>