<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<div id="page-wrapper" class="panel-body">
    <div class="table-container">
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
            <tr>
                <th>Název týmu</th>
                <th>Město</th>
                <th>Liga</th>
                <th>Body</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${teams}" var="team">
                <c:if test="${!empty team.name}">
                    <tr class="odd gradeX">
                        <td><a href="<spring:url value="/team-detail?id=${team.id}" />">${team.name}</a></td>
                        <td><a href="<spring:url value="/team-detail?id=${team.id}" />">${team.city}</a></td>
                        <td><a href="<spring:url value="/team-detail?id=${team.id}" />">${team.league.name}</a></td>
                        <td><a href="<spring:url value="/team-detail?id=${team.id}" />">${team.points}</a></td>
                    </tr>
                </c:if>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <security:authorize access="hasRole('ROLE_ADMIN')">
        <div class="button-new">
            <a href="<spring:url value="/new-team" />">
                <button type="button" class="btn btn-primary">Nový tým</button>
            </a>
        </div>
    </security:authorize>
</div>