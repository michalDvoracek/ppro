<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-md-11">
            <h3>${team.name} ${team.city}</h3>
        </div>
        <security:authorize access="hasRole('ROLE_ADMIN')">
            <div class="col-md-1">
                <a class="fa fa-pencil fa-fw" href="<spring:url value="/team-edit?id=${team.id}" />"></a>
                <a class="fa fa-trash fa-fw" href="<spring:url value="/team-remove?id=${team.id}" />"></a>
            </div>
        </security:authorize>
    </div>
    <div class="row">
        <h4> ${team.league.name}</h4>
        <h5> Body: ${team.points}</h5>
        <h5> ${team.email}</h5>
    </div>

    <div class="row">
        <h3>Zápasy</h3>
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
            <tr>
                <th>Datum</th>
                <th>Domácí</th>
                <th>Hosté</th>
                <th>Skóre (D:H)</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${matches}" var="match">
            <tr class="odd gradeX">
                    <fmt:formatDate value="${match.dateOfMatch}" type="date" pattern="dd.MM.yyyy"
                                    var="theFormattedDate"/>
                <td><a href="<spring:url value="/match-detail?id=${match.id}" />">${theFormattedDate}</a></td>
                <td>
                    <a href="<spring:url value="/match-detail?id=${match.id}" />">${match.homeTeam.name} ${match.homeTeam.city}</a>
                </td>
                <td>
                    <a href="<spring:url value="/match-detail?id=${match.id}" />">${match.awayTeam.name} ${match.awayTeam.city}</a>
                </td>
                <td><a href="<spring:url value="/match-detail?id=${match.id}" />">${match.homeGoals}
                    : ${match.awayGoals}</a></td>
                </c:forEach>
            </tbody>
        </table>
    </div>

    <div class="row">
        <h3>Hráči</h3>
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
            <tr>
                <th>Jméno</th>
                <th>Přijmení</th>
                <th>Body</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${persons}" var="person">
                <tr class="odd gradeX">
                    <td><a href="<spring:url value="/person-detail?id=${person.id}" />">${person.firstname}</a></td>
                    <td><a href="<spring:url value="/person-detail?id=${person.id}" />">${person.lastname}</a></td>
                    <td><a href="<spring:url value="/person-detail?id=${person.id}" />">${person.points}</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

    </div>

</div>
