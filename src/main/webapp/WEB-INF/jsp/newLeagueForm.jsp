<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<div id="page-wrapper" class="panel-body">
    <!-- Form Name -->
    <legend>Nová liga</legend>
    <form:form commandName="league" method="post" cssClass="form-horizontal">
        <!-- Text input-->
        <div class="form-group">
            <form:hidden path="id" class="form-control input-sm"/>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">Název ligy</label>
            <div class="col-md-4">
                <form:input path="name" cssClass="form-control input-md"/>
                <form:errors path="name"/>
            </div>
        </div>
        <!-- Button -->
        <div class="form-group">
            <div class="col-md-4">
                <input type="submit" class="btn btn-primary" value="Vytvořit" />
            </div>
        </div>
    </form:form>
</div>
