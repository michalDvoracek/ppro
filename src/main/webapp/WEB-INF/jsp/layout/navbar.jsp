<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<spring:url value="/" />"><img src="resources/img/logo.png"></a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <security:authorize access="isAuthenticated()">
            <strong><sec:authentication property="name"/></strong>
        </security:authorize>
        <!-- /.dropdown -->
        <li class="dropdown">
            <security:authorize access="!isAuthenticated()">
                <a href="<spring:url value="/login" />"><i class="fa fa-fw fa-sign-in"></i> Přihlásit</a>
            </security:authorize>
            <security:authorize access="isAuthenticated()">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <a href='<spring:url value="/person-edit"></spring:url>'><i class="fa fa-user fa-fw"></i>
                        Uživatelský profil</a>
                    <li class="divider"></li>
                    <a href='<spring:url value="/logout"></spring:url>'><i class="fa fa-fw fa-sign-out"></i> Odhlásit
                        se</a>
                </ul>
            </security:authorize>
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">

                <security:authorize access="hasRole('ROLE_USER')">
                    <li class="navbar-myteam">
                        <a href="<spring:url value="/my-team" />"><i class="fa fa-home fa-fw"></i> Můj tým</a>
                    </li>
                </security:authorize>
                <li>
                    <a href="<spring:url value="/" />"><i class="fa fa-home fa-fw"></i> Přehled</a>
                </li>
                <li>
                    <a href="<spring:url value="/matches" />"><i class="fa fa-flag fa-fw"></i> Zápasy</a>
                </li>
                <li>
                    <a href="<spring:url value="/persons" />"><i class="fa fa-user fa-fw"></i> Lidé</a>
                </li>
                <li>
                    <a href="<spring:url value="/teams" />"><i class="fa fa-group fa-fw"></i> Týmy</a>
                </li>
                <li>
                    <a href="<spring:url value="/leagues" />"><i class="fa fa-sitemap fa-fw"></i> Ligy</a>
                </li>
                <li class="navbar-login">
                    <security:authorize access="!isAuthenticated()">
                        <a href="<spring:url value="/login" />"><i class="fa fa-fw fa-sign-in"></i> Přihlásit</a>
                    </security:authorize>
                    <security:authorize access="isAuthenticated()">
                        <a href='<spring:url value="/logout"></spring:url>'><i class="fa fa-fw fa-sign-out"></i>
                            Odhlásit
                            se</a>
                    </security:authorize>
                </li>
                <security:authorize access="!isAuthenticated()">
                    <li>
                        <a href="<spring:url value="/registration" />"><i class="fa fa-fw fa-user-plus"></i>
                            Zaregistrovat se</a>
                    </li>
                </security:authorize>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
</nav>