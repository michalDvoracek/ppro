<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>


<div id="page-wrapper" class="panel-body">
    <form:form cssClass="form-horizontal" commandName="person" method="post">
        <!-- Form Name -->
        <legend>Uživatelské informace</legend>

        <form:input path="id" required="" type="hidden"/>

        <form:input path="password" required="" type="hidden"/>

        <form:input path="username" required="" type="hidden"/>

        <form:input path="points" required="" type="hidden"/>

        <security:authorize access="!hasRole('ROLE_ADMIN')">
            <form:input path="userRole.id" required="" type="hidden"/>
        </security:authorize>

        <security:authorize access="hasRole('ROLE_ADMIN')">
            <div class="form-group">
                <label class="col-md-4 control-label">Přístupové role</label>
                <div class="col-md-4">
                    <form:select path="userRole.id" cssClass="form-control">
                        <form:options items="${userRoles}" itemValue="id"
                                      itemLabel="name"></form:options>
                    </form:select>
                </div>
            </div>
        </security:authorize>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label">Křestní jméno</label>
            <div class="col-md-4">
                <form:input id="firstname" path="firstname" placeholder="Jméno" cssClass="form-control input-md"
                            required="" type="text"/>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label">Přijmení</label>
            <div class="col-md-4">
                <form:input id="lastname" path="lastname" placeholder="Přijmení" cssClass="form-control input-md"
                            required="" type="text"/>
            </div>
        </div>


        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label">Narozen</label>
            <div class="col-md-4">
                <form:input id="birth" path="birthyear" placeholder="1980" cssClass="form-control input-md"/>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label">Email</label>
            <div class="col-md-4">
                <form:input id="email" path="email" placeholder="osoba@email.com" cssClass="form-control input-md"
                            required="" typ="text"/>
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label">Telefon</label>
            <div class="col-md-4">
                <form:input id="phonenumber" path="phonenumber" placeholder="123456789"
                            cssClass="form-control input-md" required="" type="text"/>
            </div>
        </div>

        <security:authorize access="hasRole('ROLE_ADMIN')">
            <!-- Select Basic -->
            <div class="form-group">
                <label class="col-md-4 control-label">Tým</label>
                <div class="col-md-4">
                    <form:select path="team.id" cssClass="form-control">
                        <form:options id="team" name="team" items="${teams}"
                                      itemLabel="name" itemValue="id"></form:options>
                    </form:select>
                </div>
            </div>
        </security:authorize>

        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="create"></label>
            <div class="col-md-4">
                <input id="create" class="btn btn-primary" type="submit" value="Upravit"/>
            </div>
        </div>
    </form:form>
</div>


