<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="page-wrapper">
    <form:form class="form-horizontal" commandName="match" method="post">
        <fieldset>

            <!-- Form Name -->
            <legend>Nový zápas</legend>
            <div class="form-group">
                <label class="col-md-4 control-label" for="homeTeam.id">Domácí tým</label>
                <div class="col-md-4">
                    <form:select path="homeTeam.id" cssClass="form-control">
                        <form:options id="homeTeam.id" name="homeTeam.id" items="${teams}" itemValue="id"
                                      itemLabel="name" var="team"></form:options>
                    </form:select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="awayTeam.id">Tým hostí</label>
                <div class="col-md-4">
                    <form:select path="awayTeam.id" cssClass="form-control">
                        <form:options id="awayTeam.id" name="teawayTeam.id" items="${teams}" itemValue="id"
                                      itemLabel="name"></form:options>
                    </form:select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Datum zapasu</label>
                <div class="col-md-4">
                    <form:input cssClass="form-control input-md" path="dateOfMatch" type="date" required="required"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4">
                    <input id="create" class="btn btn-primary" type="submit" value="Vytvořit"/>
                </div>
            </div>
        </fieldset>
    </form:form>
</div>