<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<div class="login-form">
    <div class="form-signin">
        <div class="col-md-12">
            <h2 class="form-signin-heading">Registrace dokončena</h2>
            <div class="col-md-12 register-success">
                <span>Nyní se můžete přihlásit do systému.</span>
            </div>
            <div class="col-md-12 register-success-login">
                <a href="<spring:url value="/login" />">
                    <button class="btn btn-lg btn-primary btn-block col-md-12" type="submit">
                        Přihlásit se
                    </button>
                </a>
            </div>
            <div class="col-md-12 back-to-homepage">
                <a href="<spring:url value="/matches" />"><i class="fa fa-long-arrow-left"></i> Back to sportportal</a>
            </div>
        </div>
    </div>
</div>

