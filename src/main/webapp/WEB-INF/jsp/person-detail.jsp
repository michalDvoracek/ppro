<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div id="page-wrapper" class="panel-body">
    <div class="person-center">
        <div class="row">
            <div class="col-md-11">
                <h3>${person.firstname} ${person.lastname} </h3>
            </div>
            <div class="col-md-1">
                <div></div>
                <security:authorize access="hasRole('ROLE_ADMIN')">
                    <a class="fa fa-pencil fa-fw" href="<spring:url value="/person-edit?id=${person.id}" />"></a>
                    <a class="fa fa-trash fa-fw" href="<spring:url value="/person-remove?id=${person.id}" />"></a>
                </security:authorize>
            </div>
        </div>
        <table class="thead-person">
            <tr>
                <td class="table-person">Tým:</td>
                <td>${person.team.name}</td>
            </tr>
            <tr>
                <td class="table-person">Rok narození:</td>
                <td>${person.birthyear}</td>
            </tr>
            <tr>
                <td class="table-person">Body:</td>
                <td>${person.points}</td>
            </tr>
            <c:if test="${showMoreDetail}">
                <tr>
                    <td class="table-person">Email:</td>
                    <td>${person.email}</td>
                </tr>
                <tr>
                    <td class="table-person">Telefon:</td>
                    <td>${person.phonenumber}</td>
                </tr>
            </c:if>
            <security:authorize access="hasRole('ROLE_ADMIN')">
                <tr>
                    <td class="table-person">Login:</td>
                    <td>${person.username}</td>
                </tr>
                <tr>
                    <td class="table-person">Userrole:</td>
                    <td>${person.userRole.name}</td>
                </tr>
            </security:authorize>
        </table>
    </div>
</div>