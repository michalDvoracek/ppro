<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<body>
<div class="login-form">
    <form class="form-signin" role="form"
          action='<spring:url value="j_spring_security_check"></spring:url>'
          method="POST">
        <h2 class="form-signin-heading">Přihlášení do systému</h2>
        <div class="col-md-12">
            <input type="text" name="j_username" class="form-control"
                   placeholder="Přihlašovací jméno" required autofocus>
            <input type="password" name="j_password" class="form-control"
                   placeholder="Heslo" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">
                Přihlásit se
            </button>
        </div>
        <div class="col-md-12 back-to-homepage">
            <a href="<spring:url value="/matches" />"><i class="fa fa-long-arrow-left"></i> Back to sportportal</a>
        </div>
        <div class="col-md-12 error-message">
        <span>Přihlášení se nezdařilo, zadán špatné přihlašovací jméno nebo heslo.</span>
        </div>
    </form>
</div>
</body>