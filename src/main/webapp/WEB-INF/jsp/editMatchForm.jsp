<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<form:form class="form-horizontal" commandName="match" method="post">
    <div class="page-wrapper">
        <fieldset>
            <!-- Form Name -->
            <legend>Úprava zápasu</legend>

            <!-- Select Basic -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="selectbasic">Domácí tým</label>
                <div class="col-md-4">
                    <form:select path="homeTeam.id" cssClass="form-control">
                        <form:options id="team" name="team" items="${teams}" itemValue="id"
                                      itemLabel="name"></form:options>
                    </form:select>
                </div>
            </div>

            <!-- Select Basic -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="awayTeam">Tým hostí</label>
                <div class="col-md-4">
                    <form:select path="awayTeam.id" cssClass="form-control">
                        <form:options id="team" name="team" items="${teams}" itemValue="id"
                                      itemLabel="name"></form:options>
                    </form:select>
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="homeGoals">Góly domácí</label>
                <div class="col-md-4">
                    <form:input id="homeGoals" name="homeGoals" path="homeGoals" placeholder="0"
                                cssClass="form-control input-md"
                                required="" type="text"/>
                    <span class="help-block">Zadejte góly domácích</span>
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="awayGoals">Góly hosté</label>
                <div class="col-md-4">
                    <form:input id="awayGoals" name="awayGoals" path="awayGoals" placeholder="0"
                                cssClass="form-control input-md"
                                required="" type="text"/>
                    <span class="help-block">Zadejte góly hostí</span>
                </div>
            </div>


            <div class="form-group">
                <label class="col-md-4 control-label">Datum zapasu</label>
                <div class="col-md-4">
                    <form:input cssClass="form-control input-md" path="dateOfMatch" type="date" value="02-16-2012"
                                id="datepicker"/>
                </div>
            </div>


            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="osobaZalozit"></label>
                <div class="col-md-4">
                    <input id="create" class="btn btn-primary" type="submit" value="Upravit"/>
                </div>
            </div>
        </fieldset>
    </div>
</form:form>
