<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div id="page-wrapper">
    <div class="row">
        <div class="col-md-11">
            <h3>${match.homeTeam.league.name}</h3>
        </div>
        <security:authorize access="hasRole('ROLE_ADMIN')">
            <div class="col-md-1">
                <a class="fa fa-pencil fa-fw"
                   href="<spring:url value="/setPerson-points?homeid=${match.homeTeam.id}&awayid=${match.awayTeam.id}&matchid=${match.id}" />"></a>
                <a class="fa fa-trash fa-fw"
                   href="<spring:url value="/match-remove?id=${match.id}" />"></a>
            </div>
        </security:authorize>
    </div>
    <fmt:formatDate value="${match.dateOfMatch}" type="date" pattern="dd.MM.yyyy" var="theFormattedDate"/>
    <h4>Datum zápasu: ${theFormattedDate}<c:if test="${match.dateOfMatch >= today}">  - Zápas ještě nebyl odehrán</c:if></h4>
    <div class="row">
        <h4 class="col-md-6 left"> Domácí</h4>
        <h4 class="col-md-6"> Hosté </h4>
        <h3 class="col-md-6 left">${match.homeTeam.name}</h3>
        <h3 class="col-md-6">${match.awayTeam.name}</h3>
    </div>
    <div class="row">
        <h3 class="col-md-6 left">${match.homeGoals}</h3>
        <h3 class="col-md-6">${match.awayGoals}</h3>
    </div>
    <div>
        <h3>Diskuze:</h3>
        <c:if test="${empty comments}">
            <div class="comment-container">
                <p>Zatím nikdo nepřidal žádný komentář. Buďte první!</p>
            </div>
        </c:if>
        <c:forEach items="${comments}" var="comment">
            <div class="comment-container">
                <div class="comment-header">
                    <h6>${comment.person.firstname} ${comment.person.lastname}, <fmt:formatDate
                            value="${comment.created}" type="both"
                            pattern="hh:mm dd.MM.yyyy"/></h6>
                    <security:authorize access="hasRole('ROLE_ADMIN')">
                        <a class="fa fa-times remove-comment"
                           href="<spring:url value="/comment-remove?id=${comment.id}&matchid=${match.id}" />"></a>
                    </security:authorize>
                </div>

                <p>${comment.text}</p>

            </div>
        </c:forEach>
    </div>
    <security:authorize access="isAuthenticated()">
        <div class="add-comment">
            <form:form class="form-horizontal" commandName="comm" method="post">
                <fieldset>
                    <!-- Textarea -->
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="text">Nový komentář</label>
                        <form:textarea cssClass="form-control col-md-6" path="text" id="text" rows="5" cols="30"/>
                    </div>
                    <!-- Button -->
                    <div class="form-group col-md-4">
                        <input id="create" class="btn btn-primary" type="submit" value="Přidat komentář"/>
                    </div>
                </fieldset>
            </form:form>
        </div>
    </security:authorize>
</div>
