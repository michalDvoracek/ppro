<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<form:form cssClass="form-horizontal" commandName="person" method="post" >
<fieldset>

<!-- Form Name -->
<legend>Nová osoba</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="firstname">Křestní jméno</label>
  <div class="col-md-4">
  <form:input id="firstname" path="firstname" placeholder="Jméno" cssClass="form-control input-md" required="" type="text"/>
  <span class="help-block">Zadejte jméno osoby</span>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="osobaPrijmeni">Přijmení</label>
  <div class="col-md-4">
  <form:input id="lastname" path="lastname" placeholder="Přijmení" cssClass="form-control input-md" required="" type="text"/>
  <span class="help-block">Zadejte přijmení hráče</span>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="username">Přihlašovací jméno</label>
  <div class="col-md-4">
  <form:input id="username" path="username" placeholder="Přihlašovací jméno" cssClass="form-control input-md" required="" type="text"/>
  <span class="help-block">Zadejte uživatelské jméno</span>
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="password">Heslo</label>
  <div class="col-md-4">
    <form:input id="password" path="password" placeholder="heslo" cssClass="form-control input-md" required="" type="password"/>
    <span class="help-block">Zadejte heslo</span>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="birth">Narozen</label>
  <div class="col-md-4">
	<form:input id="birth" path="birthyear" placeholder="1980" cssClass="form-control input-md" />
  <span class="help-block">Zadejte rok narození</span>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email</label>
  <div class="col-md-4">
  <form:input id="email" path="email" placeholder="osoba@email.com" cssClass="form-control input-md" required="" typ="text"/>
  <span class="help-block">Zadejte mailovou adresu</span>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="phonenumber">Telefon</label>
  <div class="col-md-4">
  <form:input id="phonenumber" path="phonenumber" placeholder="123456789" cssClass="form-control input-md" required="" type="text"/>
  <span class="help-block">Zadejte telefonní číslo</span>
  </div>
</div>


<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="osobaTym">Tým</label>
  <div class="col-md-4">
  	<form:select path="team.id" cssClass="form-control">
  		<form:options id="team" name="team" items="${teams}" itemValue="id" itemLabel="name"></form:options>
  	</form:select>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="create"></label>
  <div class="col-md-4">
    <input id="create" class="btn btn-primary" type="submit" value="Vytvořit"/>
  </div>
</div>

</fieldset>
</form:form>
