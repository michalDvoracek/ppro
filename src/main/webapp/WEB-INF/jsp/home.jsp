<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div id="page-wrapper">
    <div class="row">
        <h2>Vítejte na sportportal !</h2>

        <h3>Nejlepší hráči</h3>
        <div class="table-container">
            <table width="100%" class="table table-striped table-bordered table-hover" id="bestPlayers">
                <thead>
                <tr>
                    <th>Jméno</th>
                    <th>Přijmení</th>
                    <th>Tým</th>
                    <th>Liga</th>
                    <th>Body</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${persons}" var="person">
                    <tr class="odd gradeX">
                        <td><a href="<spring:url value="/person-detail?id=${person.id}" />">${person.firstname}</a></td>
                        <td><a href="<spring:url value="/person-detail?id=${person.id}" />">${person.lastname}</a></td>
                        <td><a href="<spring:url value="/person-detail?id=${person.id}" />">${person.team.name}</a></td>
                        <td>
                            <a href="<spring:url value="/person-detail?id=${person.id}" />">${person.team.league.name}</a>
                        </td>
                        <td><a href="<spring:url value="/person-detail?id=${person.id}" />">${person.points}</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <h3>Nejlepší týmy dle bodů</h3>
        <div class="table-container">
            <table width="100%" class="table table-striped table-bordered table-hover" id="best-team-table">
                <thead>
                <tr>
                    <th>Název týmu</th>
                    <th>Město</th>
                    <th>Liga</th>
                    <th>Body</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${teams}" var="team">
                    <c:if test="${!empty team.name}">
                        <tr class="odd gradeX">
                            <td><a href="<spring:url value="/team-detail?id=${team.id}" />">${team.name}</a></td>
                            <td><a href="<spring:url value="/team-detail?id=${team.id}" />">${team.city}</a></td>
                            <td><a href="<spring:url value="/team-detail?id=${team.id}" />">${team.league.name}</a></td>
                            <td><a href="<spring:url value="/team-detail?id=${team.id}" />">${team.points}</a></td>
                        </tr>
                    </c:if>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <h3>Poslední zápasy</h3>
        <div class="table-container">
            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                <tr>
                    <th>liga</th>
                    <th>Datum konání</th>
                    <th>Domácí</th>
                    <th class="tdcenter">Domácí : Hostí</th>
                    <th>Hostí</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${matchesPast}" var="match">
                    <tr class="odd gradeX">
                        <fmt:formatDate value="${match.dateOfMatch}" type="date" pattern="dd.MM.yyyy"
                                        var="theFormattedDate"/>
                        <td>
                            <a href="<spring:url value="/match-detail?id=${match.id}" />">${match.homeTeam.league.name}</a>
                        </td>
                        <td>
                            <a href="<spring:url value="/match-detail?id=${match.id}" />">${theFormattedDate}</a>
                        </td>
                        <td>
                            <a href="<spring:url value="/match-detail?id=${match.id}" />">${match.homeTeam.name}</a>
                        </td>
                        <td class="tdcenter">
                            <a href="<spring:url value="/match-detail?id=${match.id}" />">${match.homeGoals} : ${match.awayGoals}</a>
                        </td>
                        <td>
                            <a href="<spring:url value="/match-detail?id=${match.id}" />">${match.awayTeam.name}</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <h3>Nejbližší zápasy</h3>
        <div class="table-container">
            <table width="100%" class="table table-striped table-bordered table-hover" id="nearest-match-table">
                <thead>
                <tr>
                    <th>liga</th>
                    <th>Datum konání</th>
                    <th>Domácí</th>
                    <th>Hostí</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${matchesFuture}" var="match">
                    <tr class="odd gradeX">
                        <fmt:formatDate value="${match.dateOfMatch}" type="date" pattern="dd.MM.yyyy"
                                        var="theFormattedDate"/>
                        <td>
                            <a href="<spring:url value="/match-detail?id=${match.id}" />">${match.homeTeam.league.name}</a>
                        </td>
                        <td><a href="<spring:url value="/match-detail?id=${match.id}" />">${theFormattedDate}</a></td>
                        <td><a href="<spring:url value="/match-detail?id=${match.id}" />">${match.homeTeam.name}</a>
                        </td>
                        <td><a href="<spring:url value="/match-detail?id=${match.id}" />">${match.awayTeam.name}</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

    </div>
</div>