<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div id="page-wrapper">
    <div class="table-container">
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
            <tr>
                <th>liga</th>
                <th>Datum konání</th>
                <th>Domácí</th>
                <th>Hostí</th>
                <th>Skóre (D:H)</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${matches}" var="match">
                <tr class="odd gradeX">
                    <fmt:formatDate value="${match.dateOfMatch}" type="date" pattern="dd-MM-yyyy"
                                    var="theFormattedDate"/>
                    <td><a href="<spring:url value="/match-detail?id=${match.id}" />">${match.homeTeam.league.name}</a>
                    </td>
                    <td><a href="<spring:url value="/match-detail?id=${match.id}" />">${theFormattedDate}</a></td>
                    <td><a href="<spring:url value="/match-detail?id=${match.id}" />">${match.homeTeam.name}</a></td>
                    <td><a href="<spring:url value="/match-detail?id=${match.id}" />">${match.awayTeam.name}</a></td>
                    <td>
                        <a href="<spring:url value="/match-detail?id=${match.id}" />">
                            <c:if test="${match.dateOfMatch < today}">
                                ${match.homeGoals}:${match.awayGoals}
                            </c:if>
                            <c:if test="${match.dateOfMatch >= today}">
                                Zápas ještě nebyl odehrán
                            </c:if>
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <security:authorize access="hasRole('ROLE_ADMIN')">
            <div class="button-new">
                <a href="<spring:url value="/new-match" />">
                    <button type="button" class="btn btn-primary">Nový zápas</button>
                </a>
            </div>
        </security:authorize>
    </div>
</div>