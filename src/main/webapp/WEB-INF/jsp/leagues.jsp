<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div id="page-wrapper">
    <div class="table-container">
        <table width="100%" class="table table-striped table-bordered table-hover" id="league-table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Název</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${leagues}" var="league">
                <tr class="odd gradeX">
                    <td><a href="<spring:url value="/league-detail?id=${league.id}" />">${league.id}</a></td>
                    <td><a href="<spring:url value="/league-detail?id=${league.id}" />">${league.name}</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="button-new">
        <security:authorize access="hasRole('ROLE_ADMIN')">
            <a href="<spring:url value="/new-league" />">
                <button type="button" class="btn btn-primary">Nová liga</button>
            </a>
        </security:authorize>
    </div>
</div>
