<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<div id="page-wrapper" class="panel-body">
    <div class="table-container">
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
            <tr>
                <th>Jméno</th>
                <th>Přijmení</th>
                <th>Tým</th>
                <th>Liga</th>
                <th>Body</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${persons}" var="person">
                <tr class="odd gradeX">
                    <td><a href="<spring:url value="/person-detail?id=${person.id}" />">${person.firstname}</a></td>
                    <td><a href="<spring:url value="/person-detail?id=${person.id}" />">${person.lastname}</a></td>
                    <td><a href="<spring:url value="/person-detail?id=${person.id}" />">${person.team.name}</a></td>
                    <td><a href="<spring:url value="/person-detail?id=${person.id}" />">${person.team.league.name}</a></td>
                   	<td><a href="<spring:url value="/person-detail?id=${person.id}" />">${person.points}</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <security:authorize access="hasRole('ROLE_ADMIN')">
        <div class="button-new">
            <a href="<spring:url value="/new-person" />">
                <button type="button" class="btn btn-primary">Nová osoba</button>
            </a>
        </div>
    </security:authorize>
</div>